// subjects
let subjects = [];
subjects["javascript"] = [];
subjects["react"] = [];
subjects["python"] = [];
subjects["java"] = [];

subjects["javascript"]["credit"] = 4;
subjects["react"]["credit"] = 7;
subjects["python"]["credit"] = 6;
subjects["java"]["credit"] = 3;

subjects["creditSum"] =
  subjects["javascript"]["credit"] +
  subjects["react"]["credit"] +
  subjects["python"]["credit"] +
  subjects["java"]["credit"];

// students
let students = [];
students["student1"] = [];
students["student2"] = [];
students["student3"] = [];
students["student4"] = [];

// student 1
//personal information
students["student1"]["personal information"] = [];
students["student1"]["personal information"]["first name"] = "Jean";
students["student1"]["personal information"]["surname"] = "Reno";
students["student1"]["personal information"]["age"] = 26;
// school attendance
students["student1"]["school attendance"] = [];
students["student1"]["school attendance"]["javascript"] = 62;
students["student1"]["school attendance"]["react"] = 57;
students["student1"]["school attendance"]["python"] = 88;
students["student1"]["school attendance"]["java"] = 90;

//sum
let jeanRenoSum =
  students["student1"]["school attendance"]["javascript"] +
  students["student1"]["school attendance"]["react"] +
  students["student1"]["school attendance"]["python"] +
  students["student1"]["school attendance"]["java"];

students["student1"]["school attendance"]["Sum"] = jeanRenoSum;
//arithmetical average
let jeanRenoArthAve = jeanRenoSum / 4;
students["student1"]["school attendance"]["ArthAve"] = jeanRenoArthAve;
//GPA
let jeanRenoGPA =
  (1 * subjects["javascript"]["credit"] +
    0.5 * subjects["react"]["credit"] +
    3 * subjects["python"]["credit"] +
    3 * subjects["java"]["credit"]) /
  subjects["creditSum"];
students["student1"]["school attendance"]["GPA"] = jeanRenoGPA;

// //console log
// console.log("Student 1:");
// console.log(
//   `Name: ${students["student1"]["personal information"]["first name"]}`
// );
// console.log(
//   `Surname: ${students["student1"]["personal information"]["surname"]}`
// );
// console.log(`Age: ${students["student1"]["personal information"]["age"]}`);
// console.log();
// console.log("School attendance:");
// console.log(
//   `Javascript: ${students["student1"]["school attendance"]["javascript"]}`
// );
// console.log(`React: ${students["student1"]["school attendance"]["react"]}`);
// console.log(`Python: ${students["student1"]["school attendance"]["python"]}`);
// console.log(`Java: ${students["student1"]["school attendance"]["java"]}`);
// console.log(`Score Sum: ${students["student1"]["school attendance"]["Sum"]}`);
// console.log(
//   `Arithmetical AVerage: ${students["student1"]["school attendance"]["ArthAve"]}`
// );
// console.log(`GPA: ${students["student1"]["school attendance"]["GPA"]}`);

// student 2
//personal information
students["student2"]["personal information"] = [];
students["student2"]["personal information"]["first name"] = "Claude";
students["student2"]["personal information"]["surname"] = "Monet";
students["student2"]["personal information"]["age"] = 19;
// school attendance
students["student2"]["school attendance"] = [];
students["student2"]["school attendance"]["javascript"] = 77;
students["student2"]["school attendance"]["react"] = 52;
students["student2"]["school attendance"]["python"] = 92;
students["student2"]["school attendance"]["java"] = 67;

//sum
let claudeMonetSum =
  students["student2"]["school attendance"]["javascript"] +
  students["student2"]["school attendance"]["react"] +
  students["student2"]["school attendance"]["python"] +
  students["student2"]["school attendance"]["java"];

students["student2"]["school attendance"]["Sum"] = claudeMonetSum;
//arithmetical average
let claudeMonetArthAve = claudeMonetSum / 4;
students["student2"]["school attendance"]["ArthAve"] = claudeMonetArthAve;
//GPA
let claudeMonetGPA =
  (2 * subjects["javascript"]["credit"] +
    0.5 * subjects["react"]["credit"] +
    4 * subjects["python"]["credit"] +
    1 * subjects["java"]["credit"]) /
  subjects["creditSum"];
students["student2"]["school attendance"]["GPA"] = claudeMonetGPA;

// //console log
// console.log();
// console.log();
// console.log("Student 2:");
// console.log(
//   `Name: ${students["student2"]["personal information"]["first name"]}`
// );
// console.log(
//   `Surname: ${students["student2"]["personal information"]["surname"]}`
// );
// console.log(`Age: ${students["student2"]["personal information"]["age"]}`);
// console.log();
// console.log("School attendance:");
// console.log(
//   `Javascript: ${students["student2"]["school attendance"]["javascript"]}`
// );
// console.log(`React: ${students["student2"]["school attendance"]["react"]}`);
// console.log(`Python: ${students["student2"]["school attendance"]["python"]}`);
// console.log(`Java: ${students["student2"]["school attendance"]["java"]}`);
// console.log(`Score Sum: ${students["student2"]["school attendance"]["Sum"]}`);
// console.log(
//   `Arithmetical AVerage: ${students["student2"]["school attendance"]["ArthAve"]}`
// );
// console.log(`GPA: ${students["student2"]["school attendance"]["GPA"]}`);

// student 3
//personal information
students["student3"]["personal information"] = [];
students["student3"]["personal information"]["first name"] = "Van";
students["student3"]["personal information"]["surname"] = "Gogh";
students["student3"]["personal information"]["age"] = 21;
// school attendance
students["student3"]["school attendance"] = [];
students["student3"]["school attendance"]["javascript"] = 51;
students["student3"]["school attendance"]["react"] = 98;
students["student3"]["school attendance"]["python"] = 65;
students["student3"]["school attendance"]["java"] = 70;

//sum
let vanGoghSum =
  students["student3"]["school attendance"]["javascript"] +
  students["student3"]["school attendance"]["react"] +
  students["student3"]["school attendance"]["python"] +
  students["student3"]["school attendance"]["java"];

students["student3"]["school attendance"]["Sum"] = vanGoghSum;
//arithmetical average
let vanGoghArthAve = vanGoghSum / 4;
students["student3"]["school attendance"]["ArthAve"] = vanGoghArthAve;
//GPA
let vanGoghGPA =
  (0.5 * subjects["javascript"]["credit"] +
    4 * subjects["react"]["credit"] +
    1 * subjects["python"]["credit"] +
    1 * subjects["java"]["credit"]) /
  subjects["creditSum"];
students["student3"]["school attendance"]["GPA"] = vanGoghGPA;

// //console log
// console.log();
// console.log();
// console.log("Student 3:");
// console.log(
//   `Name: ${students["student3"]["personal information"]["first name"]}`
// );
// console.log(
//   `Surname: ${students["student3"]["personal information"]["surname"]}`
// );
// console.log(`Age: ${students["student3"]["personal information"]["age"]}`);
// console.log();
// console.log("School attendance:");
// console.log(
//   `Javascript: ${students["student3"]["school attendance"]["javascript"]}`
// );
// console.log(`React: ${students["student3"]["school attendance"]["react"]}`);
// console.log(`Python: ${students["student3"]["school attendance"]["python"]}`);
// console.log(`Java: ${students["student3"]["school attendance"]["java"]}`);
// console.log(`Score Sum: ${students["student3"]["school attendance"]["Sum"]}`);
// console.log(
//   `Arithmetical AVerage: ${students["student3"]["school attendance"]["ArthAve"]}`
// );
// console.log(`GPA: ${students["student3"]["school attendance"]["GPA"]}`);

// student 4
//personal information
students["student4"]["personal information"] = [];
students["student4"]["personal information"]["first name"] = "Dam";
students["student4"]["personal information"]["surname"] = "Square";
students["student4"]["personal information"]["age"] = 36;
// school attendance
students["student4"]["school attendance"] = [];
students["student4"]["school attendance"]["javascript"] = 82;
students["student4"]["school attendance"]["react"] = 53;
students["student4"]["school attendance"]["python"] = 80;
students["student4"]["school attendance"]["java"] = 65;

//sum
let damSquareSum =
  students["student4"]["school attendance"]["javascript"] +
  students["student4"]["school attendance"]["react"] +
  students["student4"]["school attendance"]["python"] +
  students["student4"]["school attendance"]["java"];

students["student4"]["school attendance"]["Sum"] = damSquareSum;
//arithmetical average
let damSquareArthAve = damSquareSum / 4;
students["student4"]["school attendance"]["ArthAve"] = damSquareArthAve;
//GPA
let damSquareGPA =
  (3 * subjects["javascript"]["credit"] +
    0.5 * subjects["react"]["credit"] +
    2 * subjects["python"]["credit"] +
    1 * subjects["java"]["credit"]) /
  subjects["creditSum"];
students["student4"]["school attendance"]["GPA"] = damSquareGPA;

// //console log
// console.log();
// console.log();
// console.log("Student 4:");
// console.log(
//   `Name: ${students["student4"]["personal information"]["first name"]}`
// );
// console.log(
//   `Surname: ${students["student4"]["personal information"]["surname"]}`
// );
// console.log(`Age: ${students["student4"]["personal information"]["age"]}`);
// console.log();
// console.log("School attendance:");
// console.log(
//   `Javascript: ${students["student4"]["school attendance"]["javascript"]}`
// );
// console.log(`React: ${students["student4"]["school attendance"]["react"]}`);
// console.log(`Python: ${students["student4"]["school attendance"]["python"]}`);
// console.log(`Java: ${students["student4"]["school attendance"]["java"]}`);
// console.log(`Score Sum: ${students["student4"]["school attendance"]["Sum"]}`);
// console.log(
//   `Arithmetical AVerage: ${students["student4"]["school attendance"]["ArthAve"]}`
// );
// console.log(`GPA: ${students["student4"]["school attendance"]["GPA"]}`);

// arithmetical averages
console.log();
console.log();
console.log();
let generalArthAve =
  (jeanRenoArthAve + claudeMonetArthAve + vanGoghArthAve + damSquareArthAve) /
  4;

jeanRenoArthAve > generalArthAve
  ? console.log(
      `${students["student1"]["personal information"]["first name"]} ${students["student1"]["personal information"]["surname"]} got the Red Diploma!`
    )
  : console.log(
      `${students["student1"]["personal information"]["first name"]} ${students["student1"]["personal information"]["surname"]} is in "ვრაგ ნაროდ"!`
    );
claudeMonetArthAve > generalArthAve
  ? console.log(
      `${students["student2"]["personal information"]["first name"]} ${students["student2"]["personal information"]["surname"]} got the Red Diploma!`
    )
  : console.log(
      `${students["student2"]["personal information"]["first name"]} ${students["student2"]["personal information"]["surname"]} is in "ვრაგ ნაროდ"!`
    );
vanGoghArthAve > generalArthAve
  ? console.log(
      `${students["student3"]["personal information"]["first name"]} ${students["student3"]["personal information"]["surname"]} got the Red Diploma!`
    )
  : console.log(
      `${students["student3"]["personal information"]["first name"]} ${students["student3"]["personal information"]["surname"]} is in "ვრაგ ნაროდ"!`
    );
damSquareArthAve > generalArthAve
  ? console.log(
      `${students["student4"]["personal information"]["first name"]} ${students["student4"]["personal information"]["surname"]} got the Red Diploma!`
    )
  : console.log(
      `${students["student4"]["personal information"]["first name"]} ${students["student4"]["personal information"]["surname"]} is in "ვრაგ ნაროდ"!`
    );

// the best student with GPA
console.log();
console.log();
console.log();
if (
  students["student1"]["school attendance"]["GPA"] >
    students["student2"]["school attendance"]["GPA"] &&
  students["student1"]["school attendance"]["GPA"] >
    students["student3"]["school attendance"]["GPA"] &&
  students["student1"]["school attendance"]["GPA"] >
    students["student4"]["school attendance"]["GPA"]
) {
  console.log(
    `${students["student1"]["personal information"]["first name"]} ${students["student1"]["personal information"]["surname"]} has the highest GPA!`
  );
} else if (
  students["student2"]["school attendance"]["GPA"] >
    students["student1"]["school attendance"]["GPA"] &&
  students["student2"]["school attendance"]["GPA"] >
    students["student3"]["school attendance"]["GPA"] &&
  students["student2"]["school attendance"]["GPA"] >
    students["student4"]["school attendance"]["GPA"]
) {
  console.log(
    `${students["student2"]["personal information"]["first name"]} ${students["student2"]["personal information"]["surname"]} has the highest GPA!`
  );
} else if (
  students["student3"]["school attendance"]["GPA"] >
    students["student1"]["school attendance"]["GPA"] &&
  students["student3"]["school attendance"]["GPA"] >
    students["student2"]["school attendance"]["GPA"] &&
  students["student3"]["school attendance"]["GPA"] >
    students["student4"]["school attendance"]["GPA"]
) {
  console.log(
    `${students["student3"]["personal information"]["first name"]} ${students["student3"]["personal information"]["surname"]} has the highest GPA!`
  );
} else if (
  students["student4"]["school attendance"]["GPA"] >
    students["student1"]["school attendance"]["GPA"] &&
  students["student4"]["school attendance"]["GPA"] >
    students["student2"]["school attendance"]["GPA"] &&
  students["student4"]["school attendance"]["GPA"] >
    students["student3"]["school attendance"]["GPA"]
) {
  console.log(
    `${students["student4"]["personal information"]["first name"]} ${students["student4"]["personal information"]["surname"]} has the highest GPA!`
  );
}

//the best student with the best arithmetical average (21+)
if (
  students["student1"]["personal information"]["age"] >= 21 &&
  students["student1"]["school attendance"]["ArthAve"] >
    students["student2"]["school attendance"]["ArthAve"] &&
  students["student1"]["school attendance"]["ArthAve"] >
    students["student3"]["school attendance"]["ArthAve"] &&
  students["student1"]["school attendance"]["ArthAve"] >
    students["student4"]["school attendance"]["ArthAve"]
) {
  console.log(
    `${students["student1"]["personal information"]["first name"]} ${students["student1"]["personal information"]["surname"]} has the highest arithmetical average!`
  );
} else if (
  students["student2"]["personal information"]["age"] >= 21 &&
  students["student2"]["school attendance"]["ArthAve"] >
    students["student1"]["school attendance"]["ArthAve"] &&
  students["student2"]["school attendance"]["ArthAve"] >
    students["student3"]["school attendance"]["ArthAve"] &&
  students["student2"]["school attendance"]["ArthAve"] >
    students["student4"]["school attendance"]["ArthAve"]
) {
  console.log(
    `${students["student2"]["personal information"]["first name"]} ${students["student2"]["personal information"]["surname"]} has the highest arithmetical average!`
  );
} else if (
  students["student3"]["personal information"]["age"] >= 21 &&
  students["student3"]["school attendance"]["ArthAve"] >
    students["student1"]["school attendance"]["ArthAve"] &&
  students["student3"]["school attendance"]["ArthAve"] >
    students["student2"]["school attendance"]["ArthAve"] &&
  students["student3"]["school attendance"]["ArthAve"] >
    students["student4"]["school attendance"]["ArthAve"]
) {
  console.log(
    `${students["student3"]["personal information"]["first name"]} ${students["student3"]["personal information"]["surname"]} has the highest arithmetical average!`
  );
} else if (
  students["student4"]["personal information"]["age"] >= 21 &&
  students["student4"]["school attendance"]["ArthAve"] >
    students["student1"]["school attendance"]["ArthAve"] &&
  students["student4"]["school attendance"]["ArthAve"] >
    students["student2"]["school attendance"]["ArthAve"] &&
  students["student4"]["school attendance"]["ArthAve"] >
    students["student3"]["school attendance"]["ArthAve"]
) {
  console.log(
    `${students["student4"]["personal information"]["first name"]} ${students["student4"]["personal information"]["surname"]} has the highest arithmetical average!`
  );
}

//the best front-end developer
if (
  students["student1"]["school attendance"]["javascript"] +
    students["student1"]["school attendance"]["react"] >
    students["student2"]["school attendance"]["javascript"] +
      students["student2"]["school attendance"]["react"] &&
  students["student1"]["school attendance"]["javascript"] +
    students["student1"]["school attendance"]["react"] >
    students["student3"]["school attendance"]["javascript"] +
      students["student3"]["school attendance"]["react"] &&
  students["student1"]["school attendance"]["javascript"] +
    students["student1"]["school attendance"]["react"] >
    students["student4"]["school attendance"]["javascript"] +
      students["student4"]["school attendance"]["react"]
) {
  console.log(
    `${students["student1"]["personal information"]["first name"]} ${students["student1"]["personal information"]["surname"]} is the best front-end developer!`
  );
} else if (
  students["student2"]["school attendance"]["javascript"] +
    students["student2"]["school attendance"]["react"] >
    students["student1"]["school attendance"]["javascript"] +
      students["student1"]["school attendance"]["react"] &&
  students["student2"]["school attendance"]["javascript"] +
    students["student2"]["school attendance"]["react"] >
    students["student3"]["school attendance"]["javascript"] +
      students["student3"]["school attendance"]["react"] &&
  students["student2"]["school attendance"]["javascript"] +
    students["student2"]["school attendance"]["react"] >
    students["student4"]["school attendance"]["javascript"] +
      students["student4"]["school attendance"]["react"]
) {
  console.log(
    `${students["student2"]["personal information"]["first name"]} ${students["student2"]["personal information"]["surname"]} is the best front-end developer!`
  );
} else if (
  students["student3"]["school attendance"]["javascript"] +
    students["student3"]["school attendance"]["react"] >
    students["student2"]["school attendance"]["javascript"] +
      students["student2"]["school attendance"]["react"] &&
  students["student3"]["school attendance"]["javascript"] +
    students["student3"]["school attendance"]["react"] >
    students["student1"]["school attendance"]["javascript"] +
      students["student1"]["school attendance"]["react"] &&
  students["student3"]["school attendance"]["javascript"] +
    students["student3"]["school attendance"]["react"] >
    students["student4"]["school attendance"]["javascript"] +
      students["student4"]["school attendance"]["react"]
) {
  console.log(
    `${students["student3"]["personal information"]["first name"]} ${students["student3"]["personal information"]["surname"]} is the best front-end developer!`
  );
} else if (
  students["student4"]["school attendance"]["javascript"] +
    students["student4"]["school attendance"]["react"] >
    students["student2"]["school attendance"]["javascript"] +
      students["student2"]["school attendance"]["react"] &&
  students["student4"]["school attendance"]["javascript"] +
    students["student4"]["school attendance"]["react"] >
    students["student3"]["school attendance"]["javascript"] +
      students["student3"]["school attendance"]["react"] &&
  students["student4"]["school attendance"]["javascript"] +
    students["student4"]["school attendance"]["react"] >
    students["student1"]["school attendance"]["javascript"] +
      students["student1"]["school attendance"]["react"]
) {
  console.log(
    `${students["student4"]["personal information"]["first name"]} ${students["student4"]["personal information"]["surname"]} is the best front-end developer!`
  );
}
